extends Spatial

const o2_per_producer = 0.10
var o2_used_to_survive = 0

export var current_o2_level = 0.5
var prec_position

onready var area = $area

# Nodes
onready var bar = $o2Bar

func _process(delta):
	current_o2_level += calculate_o2(delta)
	current_o2_level = clamp(current_o2_level, 0, 1)
	update_o2_bar()

# utility functions
func change_area_radius(better_radius):
	 area.change_radius(better_radius)

func update_o2_bar():
	var gradientTexture = bar.texture as GradientTexture
	var gradient = gradientTexture.gradient
	gradient.set_offset(1, current_o2_level)

func calculate_o2(delta_time):
	var dt_o2 = o2_per_producer * area.connected_o2_sources.size() * delta_time
	dt_o2 -= o2_used_to_survive * delta_time
	
	return dt_o2

