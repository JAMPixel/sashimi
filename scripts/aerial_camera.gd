tool
extends Camera

const move_margin =  150      # in pixels
const move_speed_coef: float = 0.9     # in multiple of ground_diameter per second
const fast_move_speed_coef: float = 1.8 # in multiple of ground_diameter per second
const zoom_coef: float = 1.15
const ground_limits: Rect2 = Rect2(-500, -500, 1000, 1000)
const fast_move_delta_max: int = 300  # in pixels
const smooth_coef: float = 0.8

export(float, 5, 70) var ground_diameter = 20.0 setget set_ground_diameter
export(float, 0, 360) var yaw = 0.0 setget set_yaw
export(float, 42, 89) var pitch = 50.0 setget set_pitch
export(NodePath) var focused_node_path = null setget set_focused_node

var target_point: Vector3 = Vector3.ZERO
var smooth_target_point: Vector3 = Vector3.ZERO
var focused_node: Spatial = null
var mouse_is_in_viewport: bool = true
var middle_button_pos = null
var angle_mouse_pos = null
var angle_mouse


func _process(delta):
	if focused_node:
		target_point = focused_node.global_translation
	else:
		if not Engine.editor_hint:
			move_with_mouse(delta)
	
	clip_position()
	update_cam()


func update_cam():
	smooth_target_point = smooth_coef * smooth_target_point + (1 - smooth_coef) * target_point
	
	var cam_dist = ground_diameter / 2 / tan(deg2rad(fov / 2))
	far = cam_dist + ground_diameter * 2.5
	var dir = Vector3.FORWARD.rotated(Vector3.RIGHT, deg2rad(pitch))
	transform.origin = smooth_target_point + cam_dist * dir.rotated(Vector3.UP, deg2rad(yaw))
	transform = transform.looking_at(smooth_target_point, Vector3.UP)


func move_with_mouse(delta):
	# stop movement when the mouse is out of the window
	if not mouse_is_in_viewport:
		return
	
	var mouse_pos = get_viewport().get_mouse_position()
	var vp_size = get_viewport().size
	
	var basis = transform.basis
	# project the x and z axis of the current transform on the ground plane
	var right_dir = basis.x.slide(Vector3.UP).normalized()
	var forward_dir = basis.z.slide(Vector3.UP).normalized()
	var dir_coefs = Vector2.ZERO
	
	if Input.is_action_pressed("camera_button"):
		if Input.is_action_pressed("shift"):
			var diff = mouse_pos - middle_button_pos
			set_yaw(angle_mouse.x - diff.x)
			set_pitch(angle_mouse.y + diff.y)
		else:
			dir_coefs = (mouse_pos - middle_button_pos) / fast_move_delta_max
			if dir_coefs.length() > 1: dir_coefs = dir_coefs.normalized()
			dir_coefs *= fast_move_speed_coef
	else:
		var edge_coef = max(0, 1 - mouse_pos.x / move_margin)
		edge_coef += max(0, (mouse_pos.x - vp_size.x + move_margin) / move_margin)
		edge_coef += max(0, 1 - mouse_pos.y / move_margin)
		edge_coef += max(0, (mouse_pos.y - vp_size.y + move_margin) / move_margin)
		edge_coef = min(edge_coef, 1)
#		edge_coef = 0 # disable edge movement
		dir_coefs = edge_coef  * (mouse_pos - vp_size / 2).normalized()
		dir_coefs *= move_speed_coef
	
	#dir_coefs = dir_coefs.normalized(
	var move_vec = dir_coefs.x * abs(dir_coefs.x) * right_dir 
	move_vec += dir_coefs.y * abs(dir_coefs.y) * forward_dir
	target_point += ground_diameter * delta * move_vec


func clip_position():
	# ground limits
	if target_point.x < ground_limits.position.x: target_point.x = ground_limits.position.x
	if target_point.z < ground_limits.position.y: target_point.z = ground_limits.position.y
	if target_point.x > ground_limits.end.x:      target_point.x = ground_limits.end.x
	if target_point.z > ground_limits.end.y:      target_point.z = ground_limits.end.y


func _notification(what):
	if what == NOTIFICATION_WM_MOUSE_ENTER:
		mouse_is_in_viewport = true
	elif what == NOTIFICATION_WM_MOUSE_EXIT:
		mouse_is_in_viewport = false
		
		
func _input(event):
	if event is InputEventMouseButton and event.is_pressed():
		if event.button_index == BUTTON_WHEEL_UP:
			set_ground_diameter(ground_diameter / zoom_coef)
		if event.button_index == BUTTON_WHEEL_DOWN:
			set_ground_diameter(ground_diameter * zoom_coef)
	
	if event.is_action_pressed("camera_button"):
		middle_button_pos = get_viewport().get_mouse_position()
	elif event.is_action_released("camera_button"):
		middle_button_pos = null
	if event.is_action_pressed("shift"):
		middle_button_pos = get_viewport().get_mouse_position()
		angle_mouse = Vector2(yaw, pitch)


func set_fov_angle(val):
	fov = val

func set_ground_diameter(val):
	if val < 6: val = 6
	if val > 70: val = 70
	ground_diameter = val

func set_yaw(val):
	yaw = val
	
func set_pitch(val):
	if val < 42: val = 42
	if val > 89: val = 89
	pitch = val
	
func set_focused_node(val):
	if val is NodePath:
		focused_node = get_node(val)
	else:
		focused_node = val
