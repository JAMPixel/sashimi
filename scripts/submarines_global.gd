extends Node

var selected_submarines = []

func clear():
	for submarine in selected_submarines:
		submarine.is_selected = false
		submarine.outline.visible = false
	selected_submarines.clear()

func add(submarine):
	selected_submarines.append(submarine)
	
func erase(submarine):
	selected_submarines.erase(submarine)

func is_empty():
	return selected_submarines.empty()
