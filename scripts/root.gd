extends Spatial

onready var ground = $"nav/ground"
onready var submarines = $submarines

func _ready():
	for submarine in submarines.get_children():
		ground.connect('input_event', submarine, 'ground_input_event')
		submarine.connect('input_event', submarine, 'selection_input_event')

func _unhandled_input(event: InputEvent):
	if event.is_action_pressed("toggle_fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
