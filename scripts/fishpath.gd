tool
extends ImmediateGeometry

onready var parent = $".."

func _process(_delta):
	if(not Engine.is_editor_hint()):
		return

	if(not parent):
		return

	if(not parent.targets):
		return

	if(parent.targets.size() < 2):
		return

	clear()
	begin(Mesh.PRIMITIVE_LINES, null)
	for i in range(parent.targets.size()):
		add_vertex(parent.targets[i]-parent.targets[0])
		add_vertex(parent.targets[(i+1)%parent.targets.size()]-parent.targets[0])
	end()
