tool
extends KinematicBody

onready var nav_agent = $nav_agent
onready var lamps = $"/root/root/lamps"

export(float, 1.0, 100.0) var speed = 10.0

export(PoolVector3Array) var targets
onready var current_target = 0

func _ready():
	transform.origin = targets[current_target]
	change_target()

func _process(_delta):
	if Engine.is_editor_hint():
		transform.origin = targets[0]
		return

	if not nav_agent:
		return
		
	if not lamps.can_move(global_transform.origin):
		return
	
	var target = nav_agent.get_next_location()
	var target_vector = target - global_transform.origin
	var direction = target_vector.normalized()

	# only if target is far enough
	if(target_vector.length() > .7):
		var _pos = move_and_slide(direction * speed)

		# Fix submarine angle
		var target_angle = PI-atan2(direction.z, direction.x)
		var target_basis = Basis(Vector3.UP, target_angle)
		
		global_transform.basis = global_transform.basis.slerp(target_basis, 0.15)
	else:
		change_target()

func change_target():
	current_target = (current_target + 1) % targets.size()
	nav_agent.set_target_location(targets[current_target])
