extends Spatial

var coords_image: Image
var coords_texture: ImageTexture
var nb_lamps
onready var camera = $"../camera"
onready var object_material = load("res://materials/palette.material")
onready var ground_material = $"../nav/ground/mesh".get_active_material(0)
onready var plants_material = load("res://materials/wave.material")

func _ready():
	coords_image = Image.new()
	coords_texture = ImageTexture.new()
	coords_image.create(1000, 1, false, Image.FORMAT_RGBF)
	ground_material.set_shader_param('lamp_coords', coords_texture)


func _process(_delta):
	var i = 0
	coords_image.lock()
	for lamp in get_children():
		var pos = lamp.global_transform.origin / 1000.0 + .5 * Vector3.ONE
		#pos = camera.to_local(pos)
		coords_image.set_pixel(i, 0, Color(pos.x, pos.z, lamp.light_radius / 1000.0 + .5))
		i += 1
	coords_image.unlock()
	
	# 0 to disable MIPMAPS, REPEAT and *FILTER* for WebGL
	coords_texture.create_from_image(coords_image, 0)
	
	ground_material.set_shader_param('lamp_coords', coords_texture)
	ground_material.set_shader_param("nb_lamps", i)
	object_material.set_shader_param('lamp_coords', coords_texture)
	object_material.set_shader_param("nb_lamps", i)
	plants_material.set_shader_param('lamp_coords', coords_texture)
	plants_material.set_shader_param("nb_lamps", i)

func min_sqsdf(origin):
	var dist = 1000.0
	for lamp in get_children():
		dist = min(dist, (lamp.transform.origin - origin).length_squared() - lamp.light_radius*lamp.light_radius)
	return dist;
		
func can_move(origin):
	return min_sqsdf(origin) < 0
