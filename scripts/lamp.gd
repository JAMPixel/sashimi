extends Spatial

var light_radius = 0.0
const o2_used_to_survive = 0.005

const max_light_radius = 10.0
const delay_max_light = 0.7
const speed = 1.0 / delay_max_light
const glow_alpha_max = 0.3

var light_radius_progress = 0

onready var o2_consumer = $O2Consumer
onready var glow_material = $glow.get_active_material(0)

func _ready():
	o2_consumer.o2_used_to_survive = o2_used_to_survive

func _process(delta):
	if o2_consumer.current_o2_level > .4:
		if light_radius_progress > .99:
			return
		
		light_radius_progress += speed * delta
		if light_radius_progress > 1.0:
			light_radius_progress = 1.0
	else:
		light_radius_progress = smoothstep(0.0, 0.4, o2_consumer.current_o2_level)
		if light_radius_progress < .15:
			light_radius_progress = 0
	
	var light_coef = smoothstep(0, 1, light_radius_progress)
	light_radius = light_coef * max_light_radius
	glow_material.set_shader_param('alpha', .3 * light_coef)
