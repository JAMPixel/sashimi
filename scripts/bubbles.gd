extends StaticBody

onready var lamps = $"/root/root/lamps"

onready var particles = $particles
onready var shape = $shape

func _process(_delta):
	var active = lamps.can_move(global_transform.origin)
	particles.emitting = active
	shape.disabled = not active
