tool
extends KinematicBody

onready var nav_agent = $nav_agent
onready var lamps = $"/root/root/lamps"
onready var bubbles = $"/root/root/bubbles"
onready var o2 = $o2

export(float, 1.0, 100.0) var speed = 14.0

var target = Vector3.ZERO
var last_target_change = 0
var noise = Vector3.ZERO

enum IA_State {
	searching_source,
	searching_drain
}
var state = IA_State.searching_source

func _ready():
	pass

func _process(delta):
	if not nav_agent:
		return
		
	if not lamps:
		return

	var nearest_drain = null
	if state == IA_State.searching_source:
		if o2.current_o2_level < 1.0:
			find_o2_source()
		else:
			state = IA_State.searching_drain
	elif state == IA_State.searching_drain:
		if  o2.current_o2_level > 0.0:
			nearest_drain = find_o2_drain()
		else:
			state = IA_State.searching_source
	noisy_target(delta)

	if not lamps.can_move(global_transform.origin):
		return
	
	var target = nav_agent.get_next_location()
	var target_vector = target - global_transform.origin
	var direction = target_vector.normalized()
	var distance = target_vector.length()

	# only if target is far enough
	if(distance > .7):
		var speed_factor = 1.0
		if distance < 1.2:
			speed_factor = smoothstep(0.4, 1.2, distance) / 1.2
			
		var _pos = move_and_slide(direction * speed * speed_factor)

		# Fix submarine angle
		var target_angle = PI-atan2(direction.z, direction.x)
		var target_basis = Basis(Vector3.UP, target_angle)
		
		global_transform.basis = global_transform.basis.slerp(target_basis, 0.15)
	else:
		if state == IA_State.searching_drain:
			if nearest_drain: 
				nearest_drain.o2_consumer.current_o2_level += 0.08;
				o2.current_o2_level -= 0.08

func noisy_target(delta):
	last_target_change += delta
	if last_target_change > 1.5:
		last_target_change = 0.0
		noise = Vector3(rand_range(-4, 4), 0, rand_range(-4, 4))
		
	nav_agent.set_target_location(target + noise)

func find_o2_source():
	var nearest_source = null
	var best_distance = 1000.0
	for source in bubbles.get_children():
		var distance = (source.transform.origin - transform.origin).length_squared()
		if distance < best_distance:
			best_distance = distance
			nearest_source = source
	
	if nearest_source:
		target = nearest_source.transform.origin

func find_o2_drain():
	var nearest_lamp = null
	var best_distance = 1000.0
	for lamp in lamps.get_children():
		if 'integrated_in_submarine' in lamp and lamp.integrated_in_submarine:
			continue
		if 'o2_consumer.current_o2_level' in lamp and lamp.o2_consumer.current_o2_level < .8:
			var distance = (lamp.transform.origin - transform.origin).length_squared()
			if distance < best_distance:
				best_distance = distance
				nearest_lamp = lamp
			
	if nearest_lamp:
		target = nearest_lamp.transform.origin
	
	return nearest_lamp
