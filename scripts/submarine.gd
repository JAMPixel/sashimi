extends KinematicBody

const lampScene = preload("res://nodes/lamp.tscn")
const o2_used_to_move = 0.05
const o2_used_to_drop_lamp = 0.4
const o2_used_to_use_lamp = 0.05
const o2_used_to_survive = 0.01
const o2_refill_factor = .6

onready var o2_consumer = $o2Consumer
onready var nav_agent = $nav_agent
onready var outline = $"mesh/outline"
onready var lamps = $"../../lamps"

export(float, 1, 100) var speed = 10.0

var is_selected = false
var lamp = null

var prec_position

func _ready():
	nav_agent.set_target_location(transform.origin)
	prec_position = global_transform.origin
	o2_consumer.o2_used_to_survive = o2_used_to_survive
	
func _process(delta):
	consume_o2_while_moving(delta)
	handle_movement()
	if lamp:
		lamp.transform.origin = transform.origin
		lamp.translate(Vector3(0, 2.5, 0)) # hide this custom lamp inside submarine's mesh
		o2_consumer.current_o2_level -= o2_used_to_use_lamp * delta
		lamp.o2_consumer.current_o2_level = o2_consumer.current_o2_level

# Events
func _input(event):
	if not is_selected:
		return
	
	if event.is_action_pressed("drop_lamp"):
		spawn_lamp()
	elif event.is_action_pressed("submarine_light"):
		if not lamp:
			lamp = lampScene.instance()
			lamps.add_child(lamp)
			lamp.o2_consumer.bar.visible = false
		else:
			lamps.remove_child(lamp)
			lamp.queue_free()
			lamp = null

func ground_input_event(camera, event, position, _normal, _shape_idx):
	if event is InputEventMouseButton and event.is_pressed():
		if event.button_index == BUTTON_LEFT and is_selected:
			nav_agent.set_target_location(position)
		elif event.button_index == BUTTON_RIGHT:
			SubmarinesGlobal.clear()
			camera.set_focused_node(null)
			

func selection_input_event(camera, event, _position, _normal, _shape_idx):
	if event is InputEventMouseButton and event.is_pressed():
		if event.button_index == BUTTON_RIGHT:
			is_selected = !is_selected
			outline.visible = is_selected
			if is_selected:
				if not Input.is_key_pressed(KEY_CONTROL):
					SubmarinesGlobal.clear()	
				SubmarinesGlobal.add(self)
				camera.set_focused_node(self)
			else:
				SubmarinesGlobal.erase(self)
				if SubmarinesGlobal.is_empty():
					camera.set_focused_node(null)

# Utility

func consume_o2(quantity):
	o2_consumer.current_o2_level -= quantity

func handle_movement():
	if(not nav_agent):
		return

	var target = nav_agent.get_next_location()
	var target_vector = target - global_transform.origin
	var direction = target_vector.normalized()
	
	# only if target is far enough
	if(target_vector.length() > .5):
		var _pos = move_and_slide(direction * speed)
	
		# Fix submarine angle
		var target_angle = PI-atan2(direction.z, direction.x)
		var target_basis = Basis(Vector3.UP, target_angle)
		
		global_transform.basis = global_transform.basis.slerp(target_basis, 0.15)


func consume_o2_while_moving(delta_time):
	if prec_position != global_transform.origin:
		prec_position = global_transform.origin
		o2_consumer.current_o2_level -= o2_used_to_move * delta_time

func spawn_lamp():
	var submarine_pos = transform.origin

	# avoid placing lamps too close
	var nearest_lamp = null
	var min_distance_sq = 10000.0
	for lamp in lamps.get_children():
		var distance_sq = (lamp.transform.origin - submarine_pos).length_squared()
		if distance_sq < min_distance_sq:
			min_distance_sq = distance_sq
			nearest_lamp = lamp
	
	var o2_requirement = o2_used_to_drop_lamp
	# If there is a lamp near, adapt o2_requirement
	# and set o2_refill for future use
	var o2_refill
	if min_distance_sq < 30:
		o2_requirement *= o2_refill_factor
		o2_refill = 1.0 - nearest_lamp.o2_consumer.current_o2_level
		var o2_max_refill = (o2_consumer.current_o2_level - .1) / o2_refill_factor
		if o2_max_refill < .1:
			return
		if o2_refill > o2_max_refill:
			o2_refill = o2_max_refill
		
		o2_requirement *= o2_refill

	# check o2_requirements
	if o2_consumer.current_o2_level < o2_requirement:
		return
	
	if min_distance_sq < 30:
		nearest_lamp.o2_consumer.current_o2_level += o2_refill
		o2_consumer.current_o2_level -= o2_requirement
	else:
		var lamp = lampScene.instance()
		lamp.transform.origin = submarine_pos
		lamps.add_child(lamp)
		o2_consumer.current_o2_level -= o2_requirement
