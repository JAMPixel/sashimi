extends Spatial

var light_radius = 9.0

var follow = null

func _process(_delta):
	if not follow:
		return
	
	global_transform.origin = follow.global_transform.origin
