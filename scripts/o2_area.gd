extends Area

var connected_o2_sources = []
onready var collision_shape = $CollisionShape

func _ready():
	var _a = connect('body_entered', self, '_on_o2_consumer_body_entered')
	var _b = connect('body_exited', self, '_on_o2_consumer_body_exited')


# Events
func _on_o2_consumer_body_entered(body):
	connected_o2_sources.append(body)

func _on_o2_consumer_body_exited(body):
	connected_o2_sources.erase(body)

func change_radius(new_radius):
	collision_shape.shape.radius = new_radius
